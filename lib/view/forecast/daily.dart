import 'package:flutter/material.dart';
import 'package:flutter_application_1/func/time.dart';
import 'package:flutter_application_1/models/forecast_model.dart';

class DailyView extends StatelessWidget {
  const DailyView({
    Key key,
    @required this.futureForecast,
  }) : super(key: key);

  final Future<ForecastModel> futureForecast;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<ForecastModel>(
        future: futureForecast,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return CircularProgressIndicator();
          }
          return Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Container(
                  padding: EdgeInsets.symmetric(vertical: 18.0),
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: snapshot.data.daily.length,
                    itemBuilder: (BuildContext context, int index) => Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                width: 60.0,
                                child: Text(
                                    getOnlyDay(snapshot.data.daily[index]))),
                            Flexible(
                              flex: 1,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.cloud_sharp,
                                    size: 15.0,
                                    color: Colors.blue[500],
                                  ),
                                  Text(
                                    ' ${snapshot.data.daily[index].humidity.toStringAsFixed(0)}%',
                                    style: TextStyle(fontSize: 12),
                                  ),
                                ],
                              ),
                            ),
                            Flexible(
                              flex: 1,
                              child: Container(
                                child: Image.network(
                                  'https://openweathermap.org/img/w/${snapshot.data.daily[index].weather[0].icon}.png',
                                  height: 30.0,
                                ),
                              ),
                            ),
                            Flexible(
                                flex: 1,
                                child: Container(
                                  margin: EdgeInsets.only(top: 8.0),
                                  child: Text(
                                      '${snapshot.data.daily[index].temp.max.toStringAsFixed(0)}°/${snapshot.data.daily[index].temp.min.toStringAsFixed(0)}°'),
                                )),
                          ],
                        )
                      ],
                    ),
                  )));
        });
  }
}
