import 'package:flutter/material.dart';
import 'package:flutter_application_1/func/time.dart';
import 'package:flutter_application_1/models/forecast_model.dart';

class HourlyView extends StatelessWidget {
  const HourlyView({
    Key key,
    @required this.futureForecast,
  }) : super(key: key);

  final Future<ForecastModel> futureForecast;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<ForecastModel>(
        future: futureForecast,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return CircularProgressIndicator();
          }
          return Container(
              height: 155,
              padding: EdgeInsets.all(18),
              margin: EdgeInsets.only(top: 18),
              child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: snapshot.data.hourly.length,
                itemBuilder: (BuildContext context, int index) => Container(
                  height: 80.0,
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 18.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(getHour(snapshot.data.hourly[index])),
                        Container(
                            margin: EdgeInsets.only(top: 8.0),
                            child: Image.network(
                              'https://openweathermap.org/img/w/${snapshot.data.hourly[index].weather[0].icon}.png',
                              height: 40.0,
                            )),
                        SizedBox(
                          height: 5.0,
                        ),
                        Text(
                          "${snapshot.data.hourly[index].weather[0].main}",
                          style: TextStyle(fontSize: 14),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Text(
                          "${snapshot.data.hourly[index].temp.toStringAsFixed(0)}°",
                          style: TextStyle(fontSize: 14),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.cloud_sharp,
                              size: 15.0,
                              color: Colors.blue[500],
                            ),
                            Text(
                              ' ${snapshot.data.hourly[index].humidity}%',
                              style: TextStyle(fontSize: 12),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ));
        });
  }
}
