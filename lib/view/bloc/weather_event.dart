part of 'weather_bloc.dart';

abstract class WeatherEvent {
  const WeatherEvent();
}

class OpenDialog extends WeatherEvent {}

class CloseDialog extends WeatherEvent {}
