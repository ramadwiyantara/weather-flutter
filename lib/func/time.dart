import 'package:intl/intl.dart';

String getTime(weather) {
  var date =
      DateTime.fromMillisecondsSinceEpoch(weather.dt * 1000, isUtc: true);

  // DateTime zoneTime = date.add(Duration(seconds: weather.timezone ?? 0));

  var formattedDate = DateFormat.yMMMMEEEEd().format(date);

  return formattedDate;
}

String getHour(weather) {
  var date =
      DateTime.fromMillisecondsSinceEpoch(weather.dt * 1000, isUtc: true);

  // DateTime zoneTime = date.add(Duration(seconds: weather.timezone ?? 0));

  var formattedDate = DateFormat.j().format(date);

  return formattedDate;
}

String getOnlyDay(weather) {
  var date =
      DateTime.fromMillisecondsSinceEpoch(weather.dt * 1000, isUtc: true);

  // DateTime zoneTime = date.add(Duration(seconds: weather.timezone ?? 0));

  var formattedDate = DateFormat.EEEE().format(date);

  return formattedDate;
}
